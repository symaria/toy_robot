class GamesController < ApplicationController
  before_action :find_game, except: :new
  before_action :check_if_action_is_valid, except: [:new, :place]

  def new
  end

  def place
    GamePositioner.new(game_params, @game).place
  end

  def move
    updater.move
  end

  def left
    updater.left
  end

  def right
    updater.right
  end

  def report
    render text: PositionReporter.new(@game).call
  end

  private

  def check_if_action_is_valid
    redirect_to root_path unless @game && @game.direction
  end

  def find_game
    return true unless params[:id]
    @game = Game.find(params[:id])
  end

  def game_params
    params.require(:game).permit(:x, :y, :direction_id)
  end

  def updater
    GameUpdater.new(@game)
  end
end
