class Game < ApplicationRecord
  validates :x, :y, presence: true
  validates :x, :y, inclusion: { in: (0..4).to_a }

  belongs_to :direction

  def position_after_move_valid?
    if send(direction.axis) == direction.edge
      errors.add(:direction, 'is invalid')
      false
    else
      true
    end
  end
end


