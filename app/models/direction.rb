class Direction < ApplicationRecord
  validates :name, :axis, :edge, :move, presence: true
  validates :axis, inclusion: { in: ['x', 'y'] }
  validates :edge, inclusion: { in: [4, 0] }
  validates :move, inclusion: { in: [-1, 1] }

  def find_next
    Direction.where('id > ?', id).first || Direction.first
  end

  def find_previous
    Direction.where('id < ?', id).order('id DESC').first || Direction.last
  end
end
