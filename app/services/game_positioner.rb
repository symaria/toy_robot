class GamePositioner
  attr_reader :game_params
  attr_accessor :game

  def initialize(game_params, game = nil)
    @game_params = game_params
    @game = game || Game.new
  end

  def place
    game.assign_attributes(game_params)

    if game.valid?
      game.save
    else
      game.errors
    end
  end
end
