class PositionReporter
  attr_reader :game

  def initialize(game)
    @game = game
  end

  def call
    [game.x, game.y, game.direction.name.upcase].join(',')
  end
end
