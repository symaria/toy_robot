class GameUpdater
  attr_accessor :game

  def initialize(game)
    @game = game
  end

  def move
    return game.errors unless game.position_after_move_valid?

    game.increment!(game.direction.axis, game.direction.move)
  end

  def left
    update_direction(:find_previous)
  end

  def right
    update_direction(:find_next)
  end

  private

  def update_direction(action)
    game.update(direction: game.direction.send(action))
  end
end
