# Create directions
directions_hash = {
  north: { axis: :y, edge: 4, move: 1},
  east: { axis: :x, edge: 4, move: 1},
  south: { axis: :y, edge: 0, move: -1},
  west: { axis: :y, edge: 0, move: -1}
}

directions_hash.each do |key, values|
  Direction.find_or_create_by(name: key) do |direction|
    direction.axis = values[:axis]
    direction.edge = values[:edge]
    direction.move = values[:move]
  end
end

# Create games
5.times do
  Game.create(
    x: rand(0..4),
    y: rand(0..4),
    direction_id: rand(1..4)
  )
end
