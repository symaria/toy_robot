class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.integer :x, null: false
      t.integer :y, null: false
      t.integer :direction_id, null: false
      t.timestamps
    end
    add_index :games, :direction_id
  end
end
