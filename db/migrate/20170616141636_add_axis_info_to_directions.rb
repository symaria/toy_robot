class AddAxisInfoToDirections < ActiveRecord::Migration[5.0]
  def change
    add_column :directions, :axis, :string, null: false
    add_column :directions, :edge, :integer, null: false
    add_column :directions, :move, :integer, null: false
  end
end
