Rails.application.routes.draw do
  root to: 'games#new'

  resources :games, only: :new do
    collection do
      post '/place' => 'games#place'
      patch '/place' => 'games#place'
      patch '/move' => 'games#move'
      patch '/left' => 'games#left'
      patch '/right' => 'games#right'
      get 'report' => 'games#report'
    end
  end
end
