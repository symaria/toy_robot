FactoryGirl.define do
  factory :direction do
    name :north
    axis :y
    edge 4
    move 1
  end
end
