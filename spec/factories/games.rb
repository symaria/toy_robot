FactoryGirl.define do
  factory :game do
    x 1
    y 1
    direction
  end

  factory :game_with_invalid_direction, class: Game do
    x 4
    y 4
    direction
  end
end
