require 'rails_helper'

RSpec.describe GamesController do
  let(:valid_params) { { x: 1, y: 2, direction_id: Direction.first.id }  }
  let(:invalid_params) { { x: 5, y: 7, direction_id: Direction.first.id } }

  describe 'POST #place' do
    context 'with valid params' do
      it 'creates a new game' do
        expect do
          post :place, params: { game: valid_params }
        end.to change{ Game.count }.by(1)
      end
    end

    context 'with invalid params' do
      it 'creates a new game' do
        expect do
         post :place, params: { game: invalid_params }
        end.to_not change(Game, :count)
      end
    end

    context 'after calling another methods' do
      it 'creates a new game' do
        expect do
          patch :move
          patch :left
          post :place, params: { game: valid_params }
        end.to change{ Game.count }.by(1)
      end
    end
  end

  describe 'PATCH #place' do
    let!(:game) { create(:game) }

    context 'with valid params' do
      it 'updates the game' do
        patch :place, params: { id: game.id, game: valid_params }
        game.reload
        expect(game.y).to eq 2
      end
    end

    context 'with invalid params' do
      it 'does not update the game' do
        patch :place, params: { id: game.id, game: invalid_params }
        game.reload
        expect(game.y).to eq 1
      end
    end
  end

  describe 'PATCH #left' do
    context 'with existing game' do
      let!(:game) { create(:game) }

      it 'updates the game' do
        patch :left, params: { id: game.id }
        game.reload
        expect(game.direction.name).to eq 'west'
      end
    end

    context 'without existing game' do
      it 'redirects back' do
        expect(patch :left).to redirect_to(root_path)
      end
    end
  end

  describe 'examples' do
    context 'A' do
      it 'returns correct output' do
        params = { x: 0, y: 0, direction_id: Direction.first.id }
        post :place, params: { game: params }
        patch :move, params: { id: Game.last.id }
        patch :report, params: { id: Game.last.id }
        expect(response.body).to eq '0,1,NORTH'
      end
    end

    context 'B' do
      it 'returns correct output' do
        params = { x: 0, y: 0, direction_id: Direction.first.id }
        post :place, params: { game: params }
        patch :left, params: { id: Game.last.id }
        patch :report, params: { id: Game.last.id }
        expect(response.body).to eq '0,0,WEST'
      end
    end

    context 'C' do
      it 'returns correct output' do
        params = { x: 1, y: 2, direction_id: Direction.second.id }
        post :place, params: { game: params }
        patch :move, params: { id: Game.last.id }
        patch :move, params: { id: Game.last.id }
        patch :left, params: { id: Game.last.id }
        patch :move, params: { id: Game.last.id }
        patch :report, params: { id: Game.last.id }
        expect(response.body).to eq '3,3,NORTH'
      end
    end
  end
end
