require 'rails_helper'

RSpec.describe Direction do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:axis) }
    it { is_expected.to validate_presence_of(:edge) }
    it { is_expected.to validate_presence_of(:move) }
    it { is_expected.to validate_inclusion_of(:axis).in_array(['x', 'y']) }
    it { is_expected.to validate_inclusion_of(:edge).in_array([4, 0]) }
    it { is_expected.to validate_inclusion_of(:move).in_array([-1, 1]) }
  end

  let!(:first_dir) { Direction.first }
  let!(:second_dir) { Direction.second }
  let!(:fourth_dir) { Direction.fourth }

  describe '#find_next' do
    it 'finds next direction' do
      expect(first_dir.find_next).to eq second_dir
      expect(fourth_dir.find_next).to eq first_dir
    end
  end

  describe '#find_previous' do
    it 'finds previous direction' do
      expect(first_dir.find_previous).to eq fourth_dir
      expect(second_dir.find_previous).to eq first_dir
    end
  end
end
