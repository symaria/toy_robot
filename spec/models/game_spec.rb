require 'rails_helper'

describe Game do
  describe 'associations' do
    it { is_expected.to belong_to(:direction) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:x) }
    it { is_expected.to validate_presence_of(:y) }
    it { is_expected.to validate_inclusion_of(:x).in_array((0..4).to_a) }
    it { is_expected.to validate_inclusion_of(:y).in_array((0..4).to_a) }
  end

  describe '#position_after_move_valid?' do
    context 'with valid direction' do
      let(:game) { create(:game) }

      it 'returns true' do
        expect(game.position_after_move_valid?).to be true
        expect(game.errors.messages).to be_empty
      end
    end

    context 'with invalid direction' do
      let(:game) { create(:game_with_invalid_direction) }

      it 'returns error' do
        expect(game.position_after_move_valid?).to be false
        expect(game.errors.messages[:direction]).to eq ['is invalid']
      end
    end
  end
end
