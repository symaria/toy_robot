require 'rails_helper'

describe PositionReporter do
  describe '#call' do
    let(:game) { create(:game) }

    it 'returns game attributes' do
      expect(described_class.new(game).call).to eq '1,1,NORTH'
    end
  end
end
