require 'rails_helper'

describe GameUpdater do
  describe '#move' do
    let(:game) { create(:game, x: 4, y: 3, direction: Direction.first) }

    it 'moves a game to valid position' do
      described_class.new(game).move
      expect(game.y).to eq 4
    end

    it 'does not move a game to invalid position' do
      game.update(y: 4)
      described_class.new(game).move
      expect(game.y).to eq 4
    end
  end

  let(:game) { create(:game, direction: Direction.first) }

  describe '#left' do
    it 'changes direction to the left' do
      described_class.new(game).left
      expect(game.direction.name).to eq 'west'
    end
  end

  describe '#right' do
    it 'changes direction to the right' do
      described_class.new(game).right
      expect(game.direction.name).to eq 'east'
    end
  end
end
