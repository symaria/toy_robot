require 'rails_helper'

describe GamePositioner do
  describe '#place' do
    it 'saves a game with valid params' do
      params = { x: 1, y: 2, direction: Direction.first }

      expect do
        described_class.new(params).place
      end.to change(Game, :count).by(1)
    end

    it 'does not save an invalid game' do
      params = { x: 8, y: 5 }

      expect do
        described_class.new(params).place
      end.to_not change(Game, :count)
    end
  end
end
